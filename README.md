# Wardrobify

Team: 15

* Lauren - Hats
* Stephanie - Shoes

## Design
https://excalidraw.com/#json=jA7YTzMR4mbQ4yT01Pqnw,NiLebZVUHyoZj0DMqIUpJQ

## Shoes microservice

The models are the Shoe model and the BinVO model.  The Shoe model establishes the required fields to create a pair of shoes, and the parameters for those fiels.  The required fields are manufacturer for the shoes manufacturer, model_name for the name of the shoes, color for the color of the shoes, picture_url for a picture of the shoes, and bin for the bin where the shoes are located, which is referenced via a ForeignKey that links to the BinVO model.  The BinVO model establishes the bin as a value object, while also establishing the required fields to create a bin and the parameters for those fields.  The required fields are import href for that specific bins unique id, the closet_name for the name of the closet where the bin is located, bin_number for the number of the bin in that closet, and bin_size for the size of that bin.

Bin is a value object in the shoes microservice becuase the bin is an object in the wardrobe microservice, and in the shoes microservice it is linked back to the original lcoation use in the wardrobe microservice

## Hats microservice

The models are the Hat model, and the LocationVO model.  The Hat model establishes the required fields to create a hat, and the parameters for those fields.   The required fields are name for the name of the hat, fabric for the fabric the hat is made out of, style for the style of the hat, color for the color of the hat, pic_url for a picture of the hat, and location for the location of the hat, which is referenced via a ForeignKey that links to the LocationVO model.  The LocationVO model establishes the location as a value object, while also establishing the required fields to create a location and the parameters for those fields. The required fields are import_href for that specific locations unique id, the closet_name for the name of the closet where the hat is loacted, the section_number for the section of the closet where the hat is located, and the shelf_number for the number of the shelf in that section of the closet where the hat is located.

Location is a value object in the hats microservice because the location is an object in the wardrobe microservice, and in the hats microservice it is linked back to the original location use in the wardrobe microservice.

How to Run The Application:

Enter the following command to clone the repo into your directory:
git clone https://gitlab.com/lauren.lowe/microservice-two-shot

To create the necessary Docker containers, open Docker desktop, and enter the following commands in your terminal:

docker volume create pgdata
docker-compose build
docker-compose up

To create and test the database prior to launching the app, complete the following:

In Insomnia create a bin folder.  In that folder, create a POST http request titled create bin that is mapped to http://localhost:8100/api/bins/.  A sample of the data that should be in the JSON body is below:

        {
            "closet_name": "bintang",
            "bin_number": "2",
            "bin_size": "20"
        }

The above should create the following new bin data:

        {
            "href": "/api/bins/1/",
            "id": 1,
            "closet_name": "bintang",
            "bin_number": "2",
            "bin_size": "20"
        }

In Insomnia create a location folder.  In that folder, create a POST http request titled create location that is mapped to ttp://localhost:8100/api/locations/.  A sample of the data that should be in the JSON body is below:

        {
            "closet_name": "Bam",
            "section_number": 7,
            "shelf_number": 8
        }

The above should create the following new location data:

        {
	        "href": "/api/locations/1/",
            "id": 2,
            "closet_name": "Bam",
            "section_number": 7,
            "shelf_number": 8
        }


In Insomnia create a shoe folder.  In that folder, create a POST http request titled create shoe that is mappped to http://localhost:8080/api/shoes/.  A sample of the data that should be in the JSON body is below:
        {
            "manufacturer": "a",
            "model_name": "b",
            "color": "green",
            "picture_url": "https://encrypted-tbn1.gstatic.com/shopping?q=tbn:ANd9GcRfTFa5rNyhQ5SGHN0Z-R2TsTjJpnEPhK_nD2dLH67PNQuCytnIuMgAOaG0W-vUcnxd25rKG2w_gp0mXYUnMOBgQdmSLso&usqp=CAc",
            "bin":"/api/bins/1/"
        }

The above should create the following new shoe data:
        {
            "href": "/api/shoes/<pk:id>/",
            "manufacturer": "a",
            "model_name": "b",
            "color": "green",
            "picture_url": "https://encrypted-tbn1.gstatic.com/shopping?q=tbn:ANd9GcRfTFa5rNyhQ5SGHN0Z-R2TsTjJpnEPhK_nD2dLH67PNQuCytnIuMgAOaG0W-vUcnxd25rKG2w_gp0mXYUnMOBgQdmSLso&usqp=CAc",
            "bin": {
                "bin_size": 20,
                "bin_number": 2,
                "closet_name": "bintang",
                "import_href": "/api/bins/1/"
                }
        }

pk:id in the href field should be the shoes' respective id in the database.

In that same folder, create a DELETE http request titled delete shoe that is mapped to http://localhost:8080/api/shoes/<pk:id>/.  pk:id should be the shoes' respective id in the database.

In Insomnia create a hat folder.  In that folder, create a POST http request titled create hat that is mappped to http://localhost:8090/api/hats/.  A sample of the data that should be in the JSON body is below:
		{
			"name": "Testy",
			"fabric": "Denim",
			"style": "Trucker",
			"color": "Black",
			"pic_url": "https://m.media-amazon.com/images/I/81UGtfFVVeL._AC_UL1500_.jpg",
			"location": "/api/locations/1/"
		}

The above should create the following new hat data:
        {
            "href": "/api/hats/<pk:id>/",
            "name": "Testy",
            "fabric": "Denim",
            "style": "Trucker",
            "color": "Black",
            "pic_url": "https://m.media-amazon.com/images/I/81UGtfFVVeL._AC_UL1500_.jpg",
            "location": {
                "closet_name": "Bam",
                "import_href": "/api/locations/1/",
                "section_number": 7,
                "shelf_number": 8
            }
        }
pk:id in the href field should be the hats' respective id in the database.

In that same folder, create a DELETE http request titled delete hat that is mapped to http://localhost:8090/api/hats/<pk:id>/.  pk:id should be the hats' respective id in the database.

To open the app:
Enter the following in your web browser: http://localhost:3000/
