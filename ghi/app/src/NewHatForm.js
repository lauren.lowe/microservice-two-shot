import React from "react";

class NewHatForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            name: "",
            fabric: "",
            style: "",
            color: "",
            picUrl: "",
            location: "",
            locations: []
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeFabric = this.handleChangeFabric.bind(this);
        this.handleChangeStyle = this.handleChangeStyle.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangePicUrl = this.handleChangePicUrl.bind(this);
        this.handleChangeLocation = this.handleChangeLocation.bind(this);

    }
    async componentDidMount(){
        const url = "http://localhost:8100/api/locations/"
        const response = await fetch(url);
        if(response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.pic_url = data.picUrl;
        delete data.locations;
        delete data.picUrl;

        const hatUrl = "http://localhost:8090/api/hats/"
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(hatUrl, fetchOptions);
        if (response.ok){
          const newHat = await response.json();
            this.setState({
                name: '',
                fabric: '',
                style: '',
                color: '',
                picUrl: '',
                location: '',
            });

        }
    }

    handleChangeName(event) {
        const value = event.target.value;
        this.setState({name: value});
    }

    handleChangeFabric(event) {
        const value = event.target.value;
        this.setState({fabric: value});
    }

    handleChangeStyle(event) {
        const value = event.target.value;
        this.setState({style: value})
    }

    handleChangeColor(event) {
        const value = event.target.value;
        this.setState({color: value})
    }

    handleChangePicUrl(event) {
        const value = event.target.value;
        this.setState({picUrl: value})
    }

    handleChangeLocation(event) {
        const value = event.target.value;
        this.setState({location: value});
    }

    render() {
        return (
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Create a new hat</h1>
                  <form onSubmit={this.handleSubmit} id="new-hat-form">
                    <div className="form-floating mb-3">
                      <input onChange={this.handleChangeName} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                      <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={this.handleChangeFabric} value={this.state.fabric}placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                      <label htmlFor="fabric">Fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={this.handleChangeStyle} value={this.state.style} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                      <label htmlFor="style">Style</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={this.handleChangeColor} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                      <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={this.handleChangePicUrl} value={this.state.picUrl} placeholder="Upload Photo" required type="url" name="pic_url" id="pic_url" className="form-control" />
                      <label htmlFor="pic_url">Photo</label>
                    </div>
                    <div className="mb-3">
                      <select onChange={this.handleChangeLocation} required id="location" name="location" className="form-select">
                        <option value="">Choose a location</option>
                        {this.state.locations.map(location => {
                          return (
                            <option key={location.href} value={location.href}>{location.closet_name}</option>
                          );
                        })}
                      </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                  </form>
                </div>
              </div>
            </div>
        );
    }
}

export default NewHatForm;
