import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import HatList from './HatList';
import NewShoeForm from "./NewShoeForm"
import NewHatForm from "./NewHatForm"

function App(props) {
  if(props.shoes === undefined && props.hats === undefined){
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoesList shoes={props.shoes}/>}/>
          <Route path="hats" element={<HatList hats={props.hats}/>}/>
          <Route path="shoes/new" element={<NewShoeForm shoes = {props.shoes}/>}/>
          <Route path= "hats/new" element = {<NewHatForm hats={props.hats}/>}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
