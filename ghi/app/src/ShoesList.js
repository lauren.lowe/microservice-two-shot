import React from 'react';

class ShoesList extends React.Component{
    constructor(props){
        super(props);

        this.state= {
            shoesArray: this.props.shoes
        }
    }

    async handleDeleteShoe(shoesKey){
        var shoesKeyArr = shoesKey.split("/")
        const shoesId = shoesKeyArr[shoesKeyArr.length -2]
        const shoeUrl = "http://localhost:8080/api/shoes/" + shoesId
        const fetchOptions = {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json"
            }
        }
        const shoeResponse = await fetch(shoeUrl, fetchOptions)
        if(shoeResponse.ok){
            const response = await fetch("http://localhost:8080/api/shoes/")
                if(response.ok){
                const data = await response.json()
                this.setState({shoesArray:data.shoes})
        }
    }
}
    render(){
        return(
            <>

            <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Color</th>
                <th>Manufacturer</th>
                <th>Closet Name</th>
              </tr>
            </thead>
            <tbody>
              {this.state.shoesArray.map(shoe => {
                return (
                  <tr key={shoe.href}>
                    <td>{ shoe.model_name }</td>
                    <td>{ shoe.color }</td>
                    <td>{ shoe.manufacturer }</td>
                    <td>{ shoe.closet_name }</td>
                    <td> <span onClick={()=>{this.handleDeleteShoe(shoe.href)}}>Delete</span></td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          </>
        )
    }
}
export default ShoesList;
