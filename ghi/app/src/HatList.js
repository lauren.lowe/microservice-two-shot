import React from "react";

class HatList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hatsArray: this.props.hats
        }
        this.handleDeleteHat = this.handleDeleteHat.bind(this)
    }

    async handleDeleteHat(hatsKey) {
        var hatKeyArr = hatsKey.split("/")
        const hatId = hatKeyArr[hatKeyArr.length -2]

        const hatUrl = "http://localhost:8090/api/hats/" +hatId
        const fetchOptions = {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json"
            }
        }
        const hatResponse = await fetch(hatUrl, fetchOptions)
        if (hatResponse.ok) {
            const response = await fetch("http://localhost:8090/api/hats/")
                if(response.ok) {
                    const data= await response.json()
                    this.setState({hatsArray: data.hats})
                }
            }

        }

            render() {
                return (
                    <>
                    <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Fabric</th>
                            <th>Style</th>
                            <th>Color</th>
                            <th>Picture</th>
                            <th>Closet</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.hatsArray.map(hat => {
                        return (
                            <tr key={hat.href}>
                                <td>{ hat.name }</td>
                                <td>{ hat.fabric }</td>
                                <td>{ hat.style }</td>
                                <td>{ hat.color }</td>
                                <td>{ hat.pic_url }</td>
                                <td>{ hat.location}</td>
                                <td> <span onClick={()=>{this.handleDeleteHat(hat.href)}}>Delete</span></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            </>
                )
            }
}
export default HatList;
